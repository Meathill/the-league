<div id="comments">
  <!--PC和WAP自适应版-->
  <div id="SOHUCS" sid="<?php the_ID(); ?>"></div>
  <script type="text/javascript">
    (function () {
      var appid = 'cysUksjFg';
      var conf = 'prod_d3b2dff43d3233b2ebce8ab9dd64fe19';
      var width = window.innerWidth || document.documentElement.clientWidth;
      var head = document.getElementsByTagName("head")[0] || document.head || document.documentElement;
      var script = document.createElement("script");
      script.charset = 'UTF-8';
      script.async = true;
      if (width < 960) {
        script.id = 'changyan_mobile_js';
        script.src = 'https://changyan.sohu.com/upload/mobile/wap-js/changyan_mobile.js?client_id=' + appid + '&conf=' + conf;
        head.appendChild(script);
      } else {
        var loadJs = function (js, callback) {
          script.setAttribute("src", js);
          if (typeof callback === "function") {
            if (window.attachEvent) {
              script.onreadystatechange = function () {
                var e = script.readyState;
                if (e === "loaded" || e === "complete") {
                  script.onreadystatechange = null;
                  callback();
                }
              }
            } else {
              script.onload = callback;
            }
          }
          head.appendChild(script)
        };
        loadJs("https://changyan.sohu.com/upload/changyan.js", function () {
          window.changyan.api.config({appid: appid, conf: conf});
        });
      }
    })(); </script>
</div><!--comments-->